package com.revature.utils;

public class CassandraDBCreator {
	public CassandraDBCreator() {
		super();
	}
	
	public static void createAllTables() {
		createUsersTable();
		createApplicationsTable();
		createDocumentsTable();
		createEventsTable();
	}
	
	public static void createUsersTable() {
		StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append("user (")
				.append("employeeId int,")
				.append("active boolean,")
				.append("username text,")
				.append("role text,")
				.append("directSupervisor int,")
				.append("departmentHead int,")
				.append("reimbursementAvailable float,")
				.append("PRIMARY KEY (employeeId));");

		CassandraUtil.getInstance().getSession().execute(sb.toString());
	}

	public static void createApplicationsTable() {
		StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append("application (")
				.append("employeeId int,")
				.append("applicationId int,")
				.append("applicationStatus text,")
				.append("eventDate text,")
				.append("eventTime int,")
				.append("eventLocation text,")
				.append("eventDiscription text,")
				.append("eventCost float,")
				.append("eventType text,")
				.append("userJustification text,")
				.append("supportingDocuments list<text>,")
				.append("ds int,")
				.append("dh int,")
				.append("benco int,")
				.append("DSApproved boolean,")
				.append("DHApproved boolean,")
				.append("BenCoApproved boolean,")
				.append("lastUpdate text,")
				.append("reasonDeclined text,")
				.append("exceededAmntReason text,")
				.append("awardedAmnt float,")
				.append("dateSubmitted text,")
				.append("daysmissed int,")
				.append("PRIMARY KEY (applicationid, employeeId));");
		CassandraUtil.getInstance().getSession().execute(sb.toString());
	}
	
	public static void createDocumentsTable() {
		StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append("documents (")
				.append("fileType text,")
				.append("applicationId int,")
				.append("documentKey text,")
				.append("PRIMARY KEY (fileType, applicationId));");
		CassandraUtil.getInstance().getSession().execute(sb.toString());
	}
	
	public static void createEventsTable() {
		StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append("events (")
				.append("eventType text,")
				.append("eventCost float,")
				.append("PRIMARY KEY (eventType));");
		CassandraUtil.getInstance().getSession().execute(sb.toString());
	}
}
