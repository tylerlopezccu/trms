package com.revature.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.javalin.http.Context;

public class ControllerUtil {
	private static final Logger log = LogManager.getLogger(ControllerUtil.class);

	// Singleton Declaration
		private static ControllerUtil userController = new ControllerUtil();
		
		private static ControllerUtil controllerUtilInstance = null;

		private ControllerUtil() {
			super();
		}

		public static synchronized ControllerUtil getInstance() {
			if (controllerUtilInstance == null)
				controllerUtilInstance = new ControllerUtil();
			return controllerUtilInstance;
		}
		
	public static boolean userNotLoggedIn(Context ctx) {
		log.trace("starting userNotLoggedIn()");
		if (ctx.sessionAttribute("user") == null)
			return true;
		return false;
	}

}
