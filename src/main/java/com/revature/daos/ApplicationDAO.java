package com.revature.daos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.DefaultConsistencyLevel;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.datastax.oss.driver.api.core.cql.SimpleStatement;
import com.datastax.oss.driver.api.core.cql.SimpleStatementBuilder;
import com.revature.controllers.UserController;
import com.revature.models.Application;
import com.revature.models.Status;
import com.revature.utils.CassandraUtil;
import com.revature.utils.S3Util;

public class ApplicationDAO {
	// Variables
	private static final String APPLICATION_TABLE = "application";

	// Dependencies
	private static S3Util s3 = S3Util.getInstance();
	private static CqlSession session = CassandraUtil.getInstance().getSession();
	private static final Logger log = LogManager.getLogger(ApplicationDAO.class);

	// Singleton Declaration
	private static ApplicationDAO applicationDAOInstance = null;

	private ApplicationDAO() {
		super();
	}

	public static synchronized ApplicationDAO getInstance() {
		if (applicationDAOInstance == null)
			applicationDAOInstance = new ApplicationDAO();
		return applicationDAOInstance;
	}

	// Public methods
	public List<Application> getAllApplications() {
		log.trace("Running getAllApplications()");
		List<Application> applications = new ArrayList<Application>();

		String query = "select * from " + APPLICATION_TABLE;
		ResultSet rs = session.execute(query);
		rs.forEach(data -> {
			applications.add(buildApplication(data));
		});
		return applications;
	}

	public Application createApplication(Application app) {
		log.trace("Running createApplication()");
		String query = "Insert into " + APPLICATION_TABLE
				+ " (employeeId, applicationId, applicationStatus, eventDate, eventTime, "
				+ "eventLocation, eventDiscription, eventCost, eventType, userJustification, "
				+ "supportingDocuments, DS, DH, BenCo, DSApproved, DHApproved, BenCoApproved, lastUpdate, "
				+ "reasonDeclined, exceededAmntReason, awardedAmnt, dateSubmitted, daysmissed) "
				+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?); ";
		SimpleStatement s = new SimpleStatementBuilder(query).setConsistencyLevel(DefaultConsistencyLevel.LOCAL_QUORUM)
				.build();
		BoundStatement bound = session.prepare(s).bind(app.getEmployeeId(), generateApplicationId(),
				app.getApplicationStatus().toString(), app.getEventDate(), app.getEventTime(), app.getEventLocation(),
				app.getEventDiscription(), app.getEventCost(), app.getEventType(), app.getUserJustification(),
				app.getSupportingDocuments(), app.getDS(), app.getDH(), app.getBenCo(), app.getDSApproved(),
				app.getDHApproved(), app.getBenCoApproved(), app.getLastUpdate(), app.getReasonDeclined(),
				app.getExceededAmntReason(), app.getAwardedAmnt(), app.getDateSubmitted(), app.getDaysMissed());
		session.execute(bound);
		return app;
	}
	
	public void updateApplication(Application app) {
		log.trace("Running updateApplication()");
		String query = "update " + APPLICATION_TABLE
				+ " set applicationstatus = ?, eventdate = ?,"
				+ " eventtime = ?, eventlocation = ?, eventdiscription = ?,"
				+ " eventcost = ?, userjustification = ?, supportingdocuments = ?,"
				+ " ds = ?, dh = ?, benco = ?, dsapproved = ?, dhapproved = ?, bencoapproved = ?,"
				+ " lastupdate = ?, reasondeclined = ?, exceededamntreason = ?, awardedamnt = ?, datesubmitted = ?, daysmissed = ?"
				+ " where applicationid = ? and employeeid = ?;";
		SimpleStatement s = new SimpleStatementBuilder(query).setConsistencyLevel(DefaultConsistencyLevel.LOCAL_QUORUM)
				.build();
		BoundStatement bound = session.prepare(s).bind(
				app.getApplicationStatus().toString(), app.getEventDate(), app.getEventTime(), app.getEventLocation(),
				app.getEventDiscription(), app.getEventCost(), app.getUserJustification(),
				app.getSupportingDocuments(), app.getDS(), app.getDH(), app.getBenCo(), app.getDSApproved(),
				app.getDHApproved(), app.getBenCoApproved(), app.getLastUpdate(), app.getReasonDeclined(),
				app.getExceededAmntReason(), app.getAwardedAmnt(), app.getDateSubmitted(), app.getDaysMissed(), app.getApplicationId(), app.getEmployeeId());
		session.execute(bound);
	}

	// Private Methods
	private int generateApplicationId() {
		log.trace("generating application id");
		List<Application> apps = getAllApplications();
		if (apps == null) {
			return 0;
		}
		return apps.size();
	}

	private Application buildApplication(Row row) {
		log.trace("building application");
		// Takes in a row from CQL_DB -> builds are returns an application built from
		// the row data.
		Application app = new Application();
		app.setEmployeeId(row.getInt("employeeId"));
		app.setApplicationId(row.getInt("applicationId"));
		app.setApplicationStatus(Status.valueOf(row.getString("applicationStatus")));
		app.setEventDate(row.getString("eventDate"));
		app.setEventTime(row.getInt("eventTime"));
		app.setEventDiscription(row.getString("eventDiscription"));
		app.setEventLocation(row.getString("eventlocation"));
		app.setEventCost(row.getFloat("eventcost"));
		app.setEventType(row.getString("eventType"));
		app.setUserJustification(row.getString("userjustification"));
		app.setSupportingDocuments(row.getList("supportingdocuments", String.class));
		app.setDS(row.getInt("DS"));
		app.setDH(row.getInt("DH"));
		app.setBenCo(row.getInt("BenCo"));
		app.setDSApproved(row.getBoolean("DSApproved"));
		app.setDHApproved(row.getBoolean("DHApproved"));
		app.setBenCoApproved(row.getBoolean("BenCoApproved"));
		app.setLastUpdate(row.getString("lastUpdate"));
		app.setReasonDeclined(row.getString("reasonDeclined"));
		app.setExceededAmntReason(row.getString("exceededAmntReason"));
		app.setAwardedAmnt(row.getFloat("awardedAmnt"));
		app.setDateSubmitted(row.getString("dateSubmitted"));
		return app;
	}
}
