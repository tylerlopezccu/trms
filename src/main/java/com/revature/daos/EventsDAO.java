package com.revature.daos;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.revature.services.ApplicationService;
import com.revature.utils.CassandraUtil;

public class EventsDAO {
	private static final String EVENTS_TABLE = "events";
	private static CqlSession session = CassandraUtil.getInstance().getSession();
	private static final Logger log = LogManager.getLogger(EventsDAO.class);

	// Singleton Declaration
		private static EventsDAO instance = null;

		private EventsDAO() {
			super();
		}

		public static synchronized EventsDAO getInstance() {
			if (instance == null)
				instance = new EventsDAO();
			return instance;
		}
	
	// Public 
	public static float getEventFloat(String event) {
		log.trace("getting the events float value");
		String query = "select eventCost from " + EVENTS_TABLE + " where eventType = ?";
		BoundStatement bound = session.prepare(query).bind(event);

		ResultSet rs = session.execute(bound);
		Row r = rs.one();
		
		return r.getFloat("eventCost");
	}
	
}
