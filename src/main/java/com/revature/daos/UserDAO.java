package com.revature.daos;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.BoundStatement;
import com.datastax.oss.driver.api.core.cql.PreparedStatement;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.cql.Row;
import com.revature.models.Role;
import com.revature.models.User;
import com.revature.utils.CassandraUtil;

public class UserDAO {
	// Database Table Name
	private static final String TABLE = "user";

	// Dependencies
	private static final Logger log = LogManager.getLogger(UserDAO.class);
	private static CqlSession session = CassandraUtil.getInstance().getSession();

	// Singleton Declaration
	private static UserDAO userDAOInstance = null;

	private UserDAO() {
		super();
	}

	public static synchronized UserDAO getInstance() {
		if (userDAOInstance == null)
			userDAOInstance = new UserDAO();
		return userDAOInstance;
	}

	// Public Methods
	public static void createUser(User newUser) {
		log.trace("starting createUser()");
		String query = "Insert into " + TABLE
				+ " (employeeId, active, username, role, directSupervisor, departmentHead, reimbursementAvailable) values (?,?,?,?,?,?,?); ";
		
		PreparedStatement prepared = session.prepare(query);
		BoundStatement bound = prepared.bind(generateEmployeeId(), true, newUser.getUsername().toLowerCase(), newUser.getRole().toString(),
				newUser.getDirectSupervisor(), newUser.getDepartmentHead(), newUser.getReimbursementAvailable());
		session.execute(bound);
		log.trace("createUser() finished");
	}

	public static void updateUser(User updatedUser) {
		log.trace("starting updateUser()");
		String query = "UPDATE " + TABLE
				+ " set active = ?, username = ?, role = ?, directsupervisor = ?, reimbursementavailable = ? WHERE employeeId = ?; ";
		PreparedStatement prepared = session.prepare(query);
		BoundStatement bound = prepared.bind(updatedUser.isActive(),
				updatedUser.getUsername().toLowerCase(), updatedUser.getRole().toString(), updatedUser.getDirectSupervisor(),
				updatedUser.getReimbursementAvailable(), updatedUser.getEmployeeId());
		session.execute(bound);
		log.trace("updateUser() finished");
	}

	public static List<User> getAllUsers() {
		log.trace("starting getAllUsers()");
		List<User> users = new ArrayList<User>();
		String query = "select * from " + TABLE;
		ResultSet rs = session.execute(query);
		for (Row userData : rs) {
			User user = new User();
			user.setEmployeeId(userData.getInt("employeeId"));
			user.setActive(userData.getBoolean("active"));
			user.setUsername(userData.getString("username"));
			user.setRole(Role.valueOf(userData.getString("Role")));
			user.setDirectSupervisor(userData.getInt("directSupervisor"));
			user.setDepartmentHead(userData.getInt("departmentHead"));
			user.setReimbursementAvailable(userData.getFloat("reimbursementAvailable"));
			users.add(user);
		}
		log.trace("createUser() finished");
		return users;
	}

	public static void setReimbursementAvailable(int employeeId, float amnt) {
		log.trace("starting setReimbursementAvailable()");
		User user = getUser(employeeId);
		user.setReimbursementAvailable(user.getReimbursementAvailable() - amnt);
		updateUser(user);
	}

	// Private Methods
	private static User getUser(int employeeId) {
		log.trace("starting getUser()");

		String query = "select * from " + TABLE + " where employeeId = " + employeeId;
		ResultSet rs = session.execute(query);
		Row data = rs.one();

		User user = new User();
		user.setEmployeeId(data.getInt("employeeId"));
		user.setActive(data.getBoolean("active"));
		user.setUsername(data.getString("username"));
		user.setRole(Role.valueOf(data.getString("Role")));
		user.setDirectSupervisor(data.getInt("directSupervisor"));
		user.setDepartmentHead(data.getInt("departmentHead"));
		user.setReimbursementAvailable(data.getFloat("reimbursementAvailable"));

		return user;
	}

	private static int generateEmployeeId() {
		log.trace("generating employee id");
		return getAllUsers().size();
	}
}
