package com.revature.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.util.StringUtils;
import com.revature.Driver;
import com.revature.daos.ApplicationDAO;
import com.revature.daos.EventsDAO;
import com.revature.models.Application;
import com.revature.models.Role;
import com.revature.models.Status;
import com.revature.models.User;
import com.revature.utils.S3Util;

import io.javalin.http.UploadedFile;

public class ApplicationService {
	// Dependencies
	private static final Logger log = LogManager.getLogger(ApplicationService.class);
	private static S3Util s3;
	private static ApplicationDAO applicationDAO;
	private static EventsDAO eventDAO;

	// Singleton Declaration
	private static ApplicationService applicationServiceInstance = null;

	private ApplicationService() {
		super();
		
		 applicationDAO = ApplicationDAO.getInstance();
		 eventDAO = EventsDAO.getInstance();
		 s3 = S3Util.getInstance();
	}
	
	public ApplicationService(ApplicationDAO adMock, EventsDAO edMock, S3Util s3Mock) {
		applicationDAO = adMock;
		eventDAO = edMock;
		s3 = s3Mock;
	}

	public static synchronized ApplicationService getInstance() {
		if (applicationServiceInstance == null)
			applicationServiceInstance = new ApplicationService();
		return applicationServiceInstance;
	}

	// Public Methods
	public List<Application> getApplications(String filterBy, User user) {
		log.trace("running getApplications()");
		List<Application> apps = applicationDAO.getAllApplications();
		List<Application> temp = new ArrayList<Application>();
		for (Application app : apps) {
			if (app.getEmployeeId() == user.getEmployeeId()) temp.add(app);
		}

		switch (filterBy) {
		case "null": // return all applications
			return temp;
		case "canceled": // return applications with status "canceled"
			return temp.stream().filter(a -> a.getApplicationStatus() == Status.CANCELED).collect(Collectors.toList());
		case "inprogress": // return applications with status "inprogress"
			return temp.stream().filter(a -> a.getApplicationStatus() == Status.INPROGRESS).collect(Collectors.toList());
		case "declined": // return applications with status "declined"
			return temp.stream().filter(a -> a.getApplicationStatus() == Status.DECLINED).collect(Collectors.toList());
		case "approved": // return applications with status "approved"
			return temp.stream().filter(a -> a.getApplicationStatus() == Status.APPROVED).collect(Collectors.toList());
		case "toapprove": // return applications that a user needs to approve
			// checks to see if a user is not a base type employee
			if (!(user.getRole() == Role.EMPLOYEE)) {
				System.out.println("collecting list of applications that need to be approved by user");
				apps = applicationDAO.getAllApplications();
				List<Application> appsNeedingApproval = new ArrayList<Application>();
				for (Application app : apps) {
					// ds and dh = user
					System.out.println("checking if app DS and DH == employeeid");
					if ((app.getDS() == app.getDH()) && app.getDSApproved() == false
							&& app.getDS() == user.getEmployeeId()) {
						appsNeedingApproval.add(app);
					} else {
						if (app.getDSApproved() == false && app.getDS() == user.getEmployeeId()) {
							appsNeedingApproval.add(app);
						} else if ((app.getDSApproved() == true) && app.getDHApproved() == false && app.getDH() == user.getEmployeeId()) {
							appsNeedingApproval.add(app);
						} else if ((app.getDHApproved() == true) && app.getBenCoApproved() == false && app.getBenCo() == user.getEmployeeId()) {
							appsNeedingApproval.add(app);
						}
					}

				}
				return appsNeedingApproval;
			}
		default:
			throw new RuntimeException("Invalid filter param");
		}

	}

	public Application getApplication(int applicationId) {
		log.trace("running getApplications()");
		 List<Application> apps = applicationDAO.getAllApplications();
		 for (Application app : apps) {
			 if (app.getApplicationId() == applicationId) return app;
		 }
		 
		 return null;
	}

	public Application createApplication(Application newApp, User user) throws RuntimeException {
		log.trace("running createApplication()");
		throwErrorIfRequiredFieldsNull(newApp);

		newApp.setEmployeeId(user.getEmployeeId());
		newApp.setDS(user.getDirectSupervisor());
		newApp.setDH(user.getDepartmentHead());
		newApp.setBenCo(UserService.getUser("benco").getEmployeeId());
		newApp.setDSApproved(false);
		newApp.setDHApproved(false);
		newApp.setBenCoApproved(false);
		newApp.setLastUpdate(LocalDate.now().toString());
		newApp.setDateSubmitted(LocalDate.now().toString());
		newApp.setApplicationStatus(Status.INPROGRESS);

		return applicationDAO.createApplication(newApp);
	}

	public float getProjectedCoveredAmount(Application app) {
		log.trace("running getProjectedCoveredAmount()");
		float eventCost = app.getEventCost();
		float eventCoverage = eventDAO.getEventFloat(app.getEventType());
		return eventCost * eventCoverage;
	}
	
	public static void updateApplication(Application app) {
		log.trace("running updateApplication()");
		app.setLastUpdate(LocalDate.now().toString());
		applicationDAO.updateApplication(app);
	}

	// Private Methods

	private void throwErrorIfRequiredFieldsNull(Application app) throws RuntimeException {
		log.trace("running throwErrorIfRequiredFieldsNull()");
		String errorMessage = "";
		if (StringUtils.isNullOrEmpty(app.getEventDate())) {
			errorMessage += "eventDate ";
		}
		if (app.getEventTime() == null) {
			errorMessage += "eventTime ";
		}
		if (StringUtils.isNullOrEmpty(app.getEventLocation())) {
			errorMessage += "eventLocation ";
		}
		if (StringUtils.isNullOrEmpty(app.getEventDiscription())) {
			errorMessage += "eventDiscription ";
		}
		if (app.getEventCost() == null) {
			errorMessage += "eventCost ";
		}
		if (StringUtils.isNullOrEmpty(app.getEventType())) {
			errorMessage += "eventType ";
		}
		if (StringUtils.isNullOrEmpty(app.getUserJustification())) {
			errorMessage += "userJustification ";
		}
		if (app.getDaysMissed() == 0 || app.getDaysMissed() == null) {
			errorMessage += "daysMissed ";
		}


		if (!errorMessage.equals(""))
			throw new RuntimeException("must fill out the required fields: " + errorMessage);
	}

	public void createSupportingDocument(int applicationId, byte[] document, List<UploadedFile> files) {
		log.trace("running createSupportDocument()");
		// call s3 to store the document, then get the link from S3 for the document,
		// and store it in the application.
		List<Application> apps = applicationDAO.getAllApplications();
		Application a = null;
		for (Application app: apps) {
			if (app.getApplicationId() == applicationId) {
				a = app;
				break;
			}
		}
		
		if (a == null) {
			throw new RuntimeException("No application found");
		}
		
		for (UploadedFile file : files) {
			s3.uploadToBucket(file.getFilename(), file.getContent(), file.getSize());
			a.addSupportingDocument(file.getFilename());
		}
		
		applicationDAO.updateApplication(a);
	}
	
	// TODO :

	

	private static void updateApplicationSupervisor(Status status) {
		// approved or denied

	}

	private static void updateApplicationDepartmentHead(Status status) {
		// approved or denied

	}

	private static void updateApplicationBenco(Status status) {
		// approved or denied

	}

	public void cancelApplication(int applicationId, int employeeId) {
		return;
	}

	public List<String> getSupportingDocuments(int applicationId) {
		return null;
	}
}
