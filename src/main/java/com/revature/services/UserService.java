package com.revature.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.revature.daos.UserDAO;
import com.revature.models.Role;
import com.revature.models.User;

public class UserService {
	// Dependencies
	private static final Logger log = LogManager.getLogger(UserService.class);
	private static final UserDAO userDAO = UserDAO.getInstance();

	// Singleton Declaration
	private static UserService userServiceInstance = null;

	private UserService() {
		super();
	}

	public static synchronized UserService getInstance() {
		if (userServiceInstance == null)
			userServiceInstance = new UserService();
		return userServiceInstance;
	}

	// Public methods
	public static void createUser(User newUser) {
		log.trace("starting createUser()");		
		UserDAO.createUser(newUser);
	}

	public static List<User> getAllUsers() {
		log.trace("starting getAllUsers()");
		return UserDAO.getAllUsers();
	}

	public static User getUserByUsername(String username) throws RuntimeException {
		log.trace("starting getUserByUsername()");
		List<User> allUsers = UserDAO.getAllUsers();
		
		for (User u : allUsers) {
			if (u.getUsername().equals(username.toLowerCase())) return u;
		}
		
		throw new RuntimeException("No user found");
	}
	
	public static User getUser(String userType) {
		log.trace("starting getUser()");
		List<User> allUsers = getAllUsers();
		List<User> allUsersOfType = new ArrayList<User>();
		for(User user : allUsers) {
			if (user.getRole() == Role.valueOf(userType.toUpperCase())) allUsersOfType.add(user);
		}
		Random rand = new Random();
		int upperBound = allUsersOfType.size();
		int randomIndex = 0;
		for(int i = 0; i < 10; i++ ) {
			randomIndex = rand.nextInt(upperBound);
			System.out.println(randomIndex);
		}
		return allUsersOfType.get(randomIndex);
	}

	//	Private Methods
	
	// TODO :

	public static void deactivate(String usernameToDeactivate) {
		// TODO Auto-generated method stub

	}

	public float getReimbursementAvailable(int employeeId) {
		
		return 0;
	}

	//TODO There is a bug in the way this is being called
	public void setReimbursementAvailable(int employeeId, float newUserReimbursement) {
		log.trace("starting setReimbursementAvailable()");
		UserDAO.setReimbursementAvailable(employeeId, newUserReimbursement);
	}
}
