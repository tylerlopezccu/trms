package com.revature;

import static io.javalin.apibuilder.ApiBuilder.delete;
import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.put;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.revature.controllers.ApplicationController;
import com.revature.controllers.AuthorizingController;
import com.revature.controllers.UserController;
import com.revature.utils.CassandraDBCreator;
import com.revature.utils.CassandraUtil;
import com.revature.utils.S3Util;

import io.javalin.Javalin;

public class Driver {

	private static final Logger log = LogManager.getLogger(Driver.class);

	public static void main(String[] args) {

		log.trace("Starting Application");
		
		CassandraUtil.getInstance();
		S3Util s3 = S3Util.getInstance();

		CassandraDBCreator.createAllTables();
		
		javalin();
		
	}
	
	private static void javalin() {
		Javalin app = Javalin.create(conf -> {
			conf.requestLogger((ctx, responseTime) -> {
				log.debug(ctx.method() + " -> " + responseTime + "ms");
			});
			conf.enableDevLogging();
		}).start(8080);

		app.routes(() -> {
			path("login", () -> {
				post(AuthorizingController::login);
				delete(AuthorizingController::logout);
			});
			path("users", () -> {
				get(UserController::getAllUsers);
				post(UserController::createUser);
				path(":userId", () -> {
					get(UserController::getUser);
					put(UserController::updateUser);
					delete(UserController::deactivateUser);
				});
			});
			path("applications", () -> {
				get(ApplicationController::getApplications);
				post(ApplicationController::createApplication);
				path(":applicationId", () -> {
					get(ApplicationController::getApplication);
					put(ApplicationController::updateApplication);
					delete(ApplicationController::cancelApplication);
					path("upload", () -> {
						path("document", () -> {
							get(ApplicationController::getSupportingDocuments);
							post(ApplicationController::createSupportingDocument);
						});

					});
				});
			});
		});
	}
}
