package com.revature.models;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
	private static final Logger log = LogManager.getLogger(User.class);

	/*
	 * Server Set Variables:
	 *   employeeId
	 *   applicationId - generated in DAO
	 *   DS
	 *   DH
	 *   BenCo
	 *   DSApproved
	 *   DHApproved
	 *   BenCoApproved
	 *   LastUpdate
	 *   dateSubmitted
	 *   applicationStatus
	 *   
	 * User Set Variables
	 *   eventDate
	 *   eventTime
	 *   eventLocation
	 *   eventDiscription
	 *   eventCost
	 *   eventType
	 *   userJustification
	 *   reasonDeclined
	 *	 exceededAmntReason
	 *   awardedAmnt
	 *   
	 * Optional Variables
	 *   supportingDocuments
	 *   
	 */
	private int employeeId; //clustering key
	private int applicationId; // primary key
	private Status applicationStatus;
	private String eventDate; // yyyymmdd
	private Integer eventTime; 
	private String eventLocation;
	private String eventDiscription;
	private Float eventCost;
	private String eventType;
	private String userJustification;
	private List<String> supportingDocuments; // Optional, will be sent as a file. <s3 key, file>
	private Integer DS; // ds needed to approve the application
	private Integer DH; // dh needed to approve the application
	private Integer BenCo; // benco needed to approve the application
	private Boolean DSApproved; // has the ds approved the application
	private Boolean DHApproved; // has the dh approved the application
	private Boolean BenCoApproved; // has the benco approved the application
	private String lastUpdate; // going to be a yyyymmdd
	private String reasonDeclined;
	private String exceededAmntReason;
	private Float awardedAmnt;
	private String dateSubmitted;
	private Integer daysMissed;

	public Application() {
		super();
		log.trace("Creating ApplicationForm");
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void addSupportingDocument(String key) {
		supportingDocuments.add(key);
	}
	
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Status getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(Status status) {
		this.applicationStatus = status;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public Integer getEventTime() {
		return eventTime;
	}

	public void setEventTime(Integer eventTime) {
		this.eventTime = eventTime;
	}

	public String getEventLocation() {
		return eventLocation;
	}

	public void setEventLocation(String eventLocation) {
		this.eventLocation = eventLocation;
	}

	public String getEventDiscription() {
		return eventDiscription;
	}

	public void setEventDiscription(String eventDiscription) {
		this.eventDiscription = eventDiscription;
	}

	public Float getEventCost() {
		return eventCost;
	}

	public void setEventCost(Float eventCost) {
		this.eventCost = eventCost;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getUserJustification() {
		return userJustification;
	}

	public void setUserJustification(String userJustification) {
		this.userJustification = userJustification;
	}

	public List<String> getSupportingDocuments() {
		return supportingDocuments;
	}

	public void setSupportingDocuments(List<String> supportingDocuments) {
		this.supportingDocuments = supportingDocuments;
	}

	public Integer getDS() {
		return DS;
	}

	public void setDS(Integer dS) {
		DS = dS;
	}

	public Integer getDH() {
		return DH;
	}

	public void setDH(Integer dH) {
		DH = dH;
	}

	public Integer getBenCo() {
		return BenCo;
	}

	public void setBenCo(Integer benCo) {
		BenCo = benCo;
	}

	public Boolean getDSApproved() {
		return DSApproved;
	}

	public void setDSApproved(Boolean dSApproved) {
		DSApproved = dSApproved;
	}

	public Boolean getDHApproved() {
		return DHApproved;
	}

	public void setDHApproved(Boolean dHApproved) {
		DHApproved = dHApproved;
	}

	public Boolean getBenCoApproved() {
		return BenCoApproved;
	}

	public void setBenCoApproved(Boolean benCoApproved) {
		BenCoApproved = benCoApproved;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getReasonDeclined() {
		return reasonDeclined;
	}

	public void setReasonDeclined(String reasonDeclined) {
		this.reasonDeclined = reasonDeclined;
	}

	public String getExceededAmntReason() {
		return exceededAmntReason;
	}

	public void setExceededAmntReason(String exceededAmntReason) {
		this.exceededAmntReason = exceededAmntReason;
	}

	public Float getAwardedAmnt() {
		return awardedAmnt;
	}

	public void setAwardedAmnt(Float awardedAmnt) {
		this.awardedAmnt = awardedAmnt;
	}

	public String getDateSubmitted() {
		return dateSubmitted;
	}

	public void setDateSubmitted(String dateSubmitted) {
		this.dateSubmitted = dateSubmitted;
	}

	public Integer getDaysMissed() {
		return daysMissed;
	}

	public void setDaysMissed(Integer daysMissed) {
		this.daysMissed = daysMissed;
	}
	
	

}
