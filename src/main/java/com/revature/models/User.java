package com.revature.models;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class User {
	private static final Logger log = LogManager.getLogger(User.class);
	
	private int employeeId; // ID will be set once when user is initially created
	private boolean active = true;
	private String username;
	private Role role;
	private int directSupervisor; 
	private int departmentHead; 
	private float reimbursementAvailable = 1000f;

	public User() {
		super();
		log.trace("Creating User");
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public int getDirectSupervisor() {
		return directSupervisor;
	}

	public void setDirectSupervisor(int directSupervisor) {
		this.directSupervisor = directSupervisor;
	}

	public float getReimbursementAvailable() {
		return reimbursementAvailable;
	}

	public void setReimbursementAvailable(float f) {
		this.reimbursementAvailable = f;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public int getDepartmentHead() {
		return departmentHead;
	}

	public void setDepartmentHead(int departmentHead) {
		this.departmentHead = departmentHead;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
