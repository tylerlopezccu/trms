package com.revature.models;

public enum Status {
	INPROGRESS, DECLINED, APPROVED, CANCELED
}
