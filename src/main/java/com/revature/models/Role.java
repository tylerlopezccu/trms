package com.revature.models;

public enum Role {
	EMPLOYEE, SUPERVISOR, DEPARTMENT_HEAD, CEO, BENCO
}
