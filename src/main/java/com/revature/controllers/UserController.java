package com.revature.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.revature.models.User;
import com.revature.services.UserService;

import io.javalin.http.Context;

public class UserController {
	// Dependencies
	private static final Logger log = LogManager.getLogger(UserController.class);
	private static final UserService userService = UserService.getInstance();

	// Singleton Declaration
	private static UserController userController = new UserController();

	private static UserController UserControllerInstance = null;

	private UserController() {
		super();
	}

	public static synchronized UserController getInstance() {
		if (UserControllerInstance == null)
			UserControllerInstance = new UserController();
		return UserControllerInstance;
	}

	// public methods
	public static void createUser(Context ctx) {
		log.trace("running createUser()");

		// create and populate user object from the client request
		User newUser = new User();
		try {
			newUser = ctx.bodyAsClass(User.class);
		} catch (Exception e) {
			ctx.status(400).result("User roles must be capitalized, dont break my server again");
			return;
		}

		// call userService to create a new user,
		// if the user doesn't enter all data required, send an error message telling
		// them what they didn't enter
		try {
			userService.createUser(newUser);
		} catch (RuntimeException e) {
			log.trace("finished createUser() - Client sent bad data");
			ctx.status(400).result(e.getLocalizedMessage());
			return;
		} catch (Exception e) {
			log.error(e.getMessage());
			return;
		}

		ctx.status(204).result("User has been created successfully");
		log.trace("finished createUser() - no problems");
	}
	
	public static void getAllUsers(Context ctx) {
		log.trace("starting getAllUsers()");
		ctx.status(200).json(UserService.getAllUsers());
	}

	public static void getUser(Context ctx) {
		log.trace("Running getUser()");
		ctx.status(403).result("You cannot get a user at this time");
	}

	public static void updateUser(Context ctx) {
		log.trace("Running updateUser()");
		ctx.status(403).result("You cannot update a user at this time");
	}

	public static void deactivateUser(Context ctx) {
		log.trace("Running deactivateUser()");
		ctx.status(403).result("You cannot deactivate a user at this time");
	}

	// private methods
}
