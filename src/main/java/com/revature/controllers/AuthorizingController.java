package com.revature.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.revature.models.User;
import com.revature.services.UserService;
import com.revature.utils.ControllerUtil;

import io.javalin.http.Context;

public class AuthorizingController {
	// Dependencies
	private static final Logger log = LogManager.getLogger(AuthorizingController.class);
	private static UserService us = UserService.getInstance();

	// Singleton Declaration
	private static AuthorizingController ac = new AuthorizingController();

	private static AuthorizingController AuthorizingControllerInstance = null;

	private AuthorizingController() {
		super();
	}

	public static synchronized AuthorizingController getInstance() {
		if (AuthorizingControllerInstance == null)
			AuthorizingControllerInstance = new AuthorizingController();
		return AuthorizingControllerInstance;
	}

	// public methods
	public static void login(Context ctx) {
		log.trace("Running login()");
		// User already is logged in
		if (!ControllerUtil.userNotLoggedIn(ctx)) {
			ctx.status(200).result("You are already logged in");
			return;
		}

		User userdata;
		try {
			userdata = UserService.getUserByUsername(ctx.formParam("username"));
		} catch (RuntimeException e) {
			ctx.status(400).result(e.getLocalizedMessage());
			return;
		} catch (Exception e) {
			ctx.status(500).result("You broke my server");
			return;
		}

		// set the user session
		ctx.sessionAttribute("user", userdata);

		ctx.status(200).result("Sucessfully logged in");
	}

	public static void logout(Context ctx) {
		log.trace("Running logout()");
		ctx.sessionAttribute("user", null);
	}

	// private methods
}
