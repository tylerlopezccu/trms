package com.revature.controllers;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.amazonaws.util.StringUtils;
import com.revature.daos.EventsDAO;
import com.revature.models.Application;
import com.revature.models.Status;
import com.revature.models.User;
import com.revature.services.ApplicationService;
import com.revature.services.UserService;
import com.revature.utils.ControllerUtil;

import io.javalin.http.Context;

public class ApplicationController {
	// Dependencies
	private static final Logger log = LogManager.getLogger(ApplicationController.class);
	private static ApplicationService applicationService = ApplicationService.getInstance();
	private static UserService userService = UserService.getInstance();

	// Public Methods
	public static void getApplications(Context ctx) {
		log.trace("Running getApplications()");

		User user = ctx.sessionAttribute("user");
		String filter = ctx.formParam("filter");

		// checks if user is logged in
		if (user == null) {
			ctx.status(401).result("Unauthorized");
			return;
		}

		List<Application> apps = applicationService.getApplications(filter, user);
		ctx.status(200).json(apps);
	}

	public static void getApplication(Context ctx) {
		log.trace("Running getApplication()");

		User user = ctx.sessionAttribute("user");

		// checks if user is logged in
		if (user == null) {
			ctx.status(401).result("Unauthorized");
			return;
		}

		int applicationId = Integer.getInteger(ctx.pathParam("applicationId"));
		Application app = applicationService.getApplication(applicationId);
		if (app == null) {
			ctx.status(204).result("No application found");
			return;
		}

		ctx.status(200).json(app);
	}

	public static void createApplication(Context ctx) {
		log.trace("Running createApplication()");

		User user = ctx.sessionAttribute("user");

		// checks if user is logged in
		if (user == null) {
			ctx.status(401).result("Unauthorized");
			return;
		}

		Application newApp = ctx.bodyAsClass(Application.class);

		try {
			newApp = applicationService.createApplication(newApp, user);
		} catch (RuntimeException e) {
			// incomplete user object
			e.printStackTrace();
			ctx.status(400).result(e.getLocalizedMessage());
			return;
		} catch (Exception e) {
			log.error(e.getMessage());
			ctx.status(500).result("You broke something");
			return;
		}
		userService.setReimbursementAvailable(user.getEmployeeId(),
				applicationService.getProjectedCoveredAmount(newApp));

		ctx.status(200).result(StringUtils.fromFloat(applicationService.getProjectedCoveredAmount(newApp)));

	}

	public static void updateApplication(Context ctx) {
		log.trace("Running updateApplication()");

		if (ControllerUtil.userNotLoggedIn(ctx)) {
			ctx.status(401).result("You must be logged in to access applications");
			return;
		}

		User user = ctx.sessionAttribute("user");
		int appIdToUpdate = Integer.parseInt(ctx.pathParam("applicationId"));
		Application app = applicationService.getApplication(appIdToUpdate);
		boolean approved = Boolean.parseBoolean(ctx.formParam("approved"));

		if ((app.getDS() == app.getDH()) && app.getDSApproved() == false && app.getDS() == user.getEmployeeId()) {
			if (approved) {
				app.setDSApproved(true);
				app.setDHApproved(true);
			} else {
				app.setApplicationStatus(Status.DECLINED);
				app.setReasonDeclined(ctx.formParam("reason"));
			}
		} else {
			if (app.getDSApproved() == false && app.getDS() == user.getEmployeeId()) {
				if (approved) {
					app.setDSApproved(true);
				} else {
					app.setApplicationStatus(Status.DECLINED);
					app.setReasonDeclined(ctx.formParam("reason"));
				}
			} else if ((app.getDSApproved() == true) && app.getDHApproved() == false && app.getDH() == user.getEmployeeId()) {
				if (approved) {
					app.setDHApproved(true);
				} else {
					app.setApplicationStatus(Status.DECLINED);
					app.setReasonDeclined(ctx.formParam("reason"));
				}
			} else if ((app.getDHApproved() == true) && app.getBenCoApproved() == false && app.getBenCo() == user.getEmployeeId()) {
				if (approved) {
					app.setBenCoApproved(true);
					app.setApplicationStatus(Status.APPROVED);
					if (ctx.formParam("amnt") != null) {
						// set the user's total available reimbursement amount updated with the benco's approval amnt
						app.setAwardedAmnt(Float.parseFloat(ctx.formParam("amnt")));
						app.setExceededAmntReason(ctx.formParam("amntExceededReason"));
					} else {
						String type = app.getEventType();
						float ftype = EventsDAO.getEventFloat(type);
						app.setAwardedAmnt(app.getEventCost() * ftype);
					}
				} else {
					app.setApplicationStatus(Status.DECLINED);
					app.setReasonDeclined(ctx.formParam("reason"));
				}
			}
		}

		ApplicationService.updateApplication(app);
		
		ctx.status(200).result("Application was successfully updated");

	}

	// Private Methods

	// TODO :

	public static void cancelApplication(Context ctx) {
		log.trace("Running cancelApplication()");

		if (ControllerUtil.userNotLoggedIn(ctx)) {
			ctx.status(401).result("You must be logged in to access applications");
			return;
		}

		User loggedInUser = ctx.sessionAttribute("user");
		int applicationToCancel = Integer.getInteger(ctx.pathParam("applicationId"));

		applicationService.cancelApplication(applicationToCancel, loggedInUser.getEmployeeId());
		ctx.status(200).result("Application status is now canceled");
	}

	public static void getSupportingDocuments(Context ctx) {
		log.trace("Running getSupportingDocuments()");
		if (ControllerUtil.userNotLoggedIn(ctx)) {
			ctx.status(401).result("You must be logged in to access applications");
			return;
		}

		int applicationId = Integer.parseInt(ctx.pathParam("applicationId"));

		ctx.status(200).json(applicationService.getSupportingDocuments(applicationId));
	}

	public static void createSupportingDocument(Context ctx) {
		log.trace("Running createSupportDocument()");
		if (ControllerUtil.userNotLoggedIn(ctx)) {
			ctx.status(401).result("You must be logged in to upload supporting documents");
			return;
		}

		int applicationId = Integer.parseInt(ctx.pathParam("applicationId"));
		byte[] document = ctx.bodyAsBytes();
		try {
			applicationService.createSupportingDocument(applicationId, document, ctx.uploadedFiles());
		} catch (RuntimeException e) {
			ctx.status(400).result(e.getLocalizedMessage());
			return;
		}
		ctx.status(200).result("Document(s) created and stored successfully");
	}

	public static boolean autoApproveApplication() {
		log.trace("Running autoApproveApplication()");
		// call and when an application is > 1 day in DS or DH, increment to next stage.
		return true;
	}

}
