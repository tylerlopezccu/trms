
# Tuition Reimbursement Management System


# Project Description
Software to be used to track company reimbursements for continuous learning.


# Technologies Used
* Java
* RESTful API
* Javalin
* JUnit
* Log4J
* Postman
* NoSQL (Cassandra)
* AWS S3
* AWS Keyspaces

# Getting Started
* Clone the github repo
* Setup AWS Keyspaces
